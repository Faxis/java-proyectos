package fabricaschalco;

import java.util.Scanner;

public class FabricasChalco {

    public static void main(String[] args) {
        Scanner x = new Scanner(System.in);
        System.out.println("///// usando setters /////");
        Pago pago = new Pago();
        System.out.println("Ingresa Id de empleado: ");
        pago.setEmpleado(x.nextInt());

        System.out.println("Ingresa Venta total: ");
        pago.setVenta_total(x.nextDouble());

        System.out.println("Ingresa Sueldo base");
        pago.setSueldo_base(x.nextDouble());

        System.out.println("++++++ Ticket ++++++");
        System.out.println("+ Frabricas Chalco +");
        System.out.println("++++++++++++++++++++");

        System.out.println("ID: " + pago.getEmpleado());
        System.out.println("Venta Total: " + pago.getVenta_total());
        System.out.println("Sueldo base: " + pago.getSueldo_base());
        System.out.println("Comision (%7): " + pago.getComision());
        System.out.println("++++++++++++++++++++");
        System.out.println("Sueldo Total $" + pago.getSueldo_Total());
        System.out.println("++++++++++++++++++++");

        System.out.println("///// usando contructor /////");

        System.out.println("Ingresa Id de empleado: ");
        int id = x.nextInt();
        System.out.println("Ingresa Venta total: ");
        double venta = x.nextDouble();
        System.out.println("Ingresa Sueldo base");
        double sueldo_base = x.nextDouble();

        pago = new Pago(id, venta, sueldo_base);

        System.out.println("++++++ Ticket ++++++");
        System.out.println("+ Frabricas Chalco +");
        System.out.println("++++++++++++++++++++");

        System.out.println("ID: " + pago.getEmpleado());
        System.out.println("Venta Total: " + pago.getVenta_total());
        System.out.println("Sueldo base: " + pago.getSueldo_base());
        System.out.println("Comision (%7): " + pago.getComision());
        System.out.println("++++++++++++++++++++");
        System.out.println("Sueldo Total $" + pago.getSueldo_Total());
        System.out.println("++++++++++++++++++++");

    }

}
