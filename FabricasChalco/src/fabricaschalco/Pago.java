package fabricaschalco;

public class Pago {

    private int empleado;
    private double venta_total;
    private double sueldo_base;
    private double comision;

    public Pago() {

    }

    public Pago(int empleado, double venta_total, double sueldo_base) {
        this.empleado = empleado;
        this.venta_total = venta_total;
        this.sueldo_base = sueldo_base;
    }

    public int getEmpleado() {
        return empleado;
    }

    public double getVenta_total() {
        return venta_total;
    }

    public double getSueldo_base() {
        return sueldo_base;
    }

    public double getComision() {
        comision = venta_total * 0.07;
        return comision;
    }

    public void setEmpleado(int empleado) {
        this.empleado = empleado;
    }

    public void setVenta_total(double venta_total) {
        this.venta_total = venta_total;
    }

    public void setSueldo_base(double sueldo_base) {
        this.sueldo_base = sueldo_base;
    }

    public double getSueldo_Total() {
        return getComision() + getSueldo_base();
    }

}
