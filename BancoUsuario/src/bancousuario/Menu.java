package bancousuario;

import java.util.Scanner;

public class Menu {

    Scanner x = new Scanner(System.in);
    private final String usuario;
    private final String clave;
    private double dinero;

    public Menu(String usuario, String clave, double dinero) {
        this.usuario = usuario;
        this.clave = clave;
        this.dinero = dinero;
    }

    public void mostrarMenu() {
        int opcion;

        do {
            System.out.println("Que deseas hacer?\n");

            System.out.println("1 - Consultar Saldo");
            System.out.println("2 - Retirar Dinero");
            System.out.println("3 - Depositar Dinero");
            System.out.println("4 - Salir");

            System.out.print(">>");
            opcion = x.nextInt();

            switch (opcion) {
                case 1:
                    this.consultar_saldo();
                    break;
                case 2:
                    this.retirar_dinero();
                    break;
                case 3:
                    this.depositar_dinero();
                    break;
                case 4:
                    this.salir();
                    break;
                default:
                    System.out.println("Esa opcion no existe");
                    break;
            }
        } while (!((opcion == 1 && opcion == 2 && opcion == 3)));

    }

    public void consultar_saldo() {

        System.out.println("@" + this.usuario + " @Su saldo es: " + this.dinero);

    }

    public void retirar_dinero() {
        double retirar;

        System.out.println("Cuanto dinero desea retirar?");
        System.out.print(">>");
        retirar = x.nextDouble();
        System.out.print("$");

        if (retirar > this.dinero) {
            System.out.println("Lo sentimos, no puede retirar esa cantidad de dinero");
        } else {
            this.dinero = dinero - retirar;
            System.out.println("Se ha retirado $" + retirar + " De su cuenta satisfactoriamente");
        }
    }

    public void depositar_dinero() {
        double depositar;
        System.out.println("#Cuanto deseas depositar?");
        System.out.print(">>");
        depositar = x.nextDouble();

        if (depositar > 0) {
            this.dinero = this.dinero + depositar;
            System.out.println("Se a agregado $" + depositar + " a tu estado de cuenta");
        } else {
            System.out.println("No puede depositar la cantidad " + depositar + "$ @Consulte su estado de cuenta");
        }

    }

    public void salir() {
        System.out.println("Gracias por usar el banco, vuelve pronto ;)");
        System.exit(0);

    }

}
