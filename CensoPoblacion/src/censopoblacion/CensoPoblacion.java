package censopoblacion;

import java.util.Scanner;

public class CensoPoblacion {

    public static void main(String[] args) {
        Scanner x = new Scanner(System.in);
        //La constante K
        double k = 0.03465735903;

        //Poblaciones
        double p1 = 0;
        //Años y calculo
        double a1 = 0, a2 = 0, a3 = 0;

        System.out.println("Ingresa Year (A) principal; ");
        a1 = x.nextDouble();
        
        System.out.println("Ingresa su poblacion de "+a1);
        p1 = x.nextDouble();

        System.out.println("Ingresa Year (B) principal; ");
        a2 = x.nextDouble();

        System.out.println("Ingresa Year (C) principal; ");
        a3 = x.nextDouble();

        double fecha = a2 - a1;
        // (Y) y su poblacion 1
        System.out.print("Para el año " + a1 + " Poblacion A = ");
        System.out.println((p1 * Math.pow(Math.E, (k * 0))));
        // (Y) y su poblacion 2
        System.out.print("Para el año " + a2 + " Poblacion B = ");
        System.out.println((p1 * Math.pow(Math.E, (k * fecha))));

        //Poblacion final en diferente año
        System.out.print("Para el año " + a3 + " Poblacion C ");
        System.out.println((p1 * Math.pow(Math.E, (k * (a3 - a1)))));

    }

}
