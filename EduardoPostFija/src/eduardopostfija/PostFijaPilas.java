package eduardopostfija;

import java.util.Scanner;
import java.util.Stack;

public class PostFijaPilas {

    String operadores[][] = {{"-", "+"}, {"/", "*"}, {"%", "^"}, {"sin", "cos", "tan", "log"}, {"(", ")"}};
    String conservar;
    int fila, anterior = 0, re_fila;
    boolean bool = false;

    String variables[] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
        "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "pi"};

    String cadena = "1+3*x/7+5*cos(pi*x^5-3)+5";

    Stack pila_operadores = new Stack();
    Stack pila_temp = new Stack();
    Stack pila_final = new Stack();
    Stack pila_reverso = new Stack();

    Scanner x = new Scanner(System.in);

    public PostFijaPilas() {

    }

    public void conversor_caracteres() {

        for (int i = 0; i <= (int) cadena.chars().count() - 1; i++) {

            if ((cadena.charAt(i) == 's' && cadena.charAt(i + 1) == 'i') && (cadena.charAt(i + 2) == 'n')) {
                i += 3;
                pila_operadores.add("sin");
            } else if ((cadena.charAt(i) == 'c' && cadena.charAt(i + 1) == 'o') && (cadena.charAt(i + 2) == 's')) {
                i += 3;
                pila_operadores.add("cos");
            } else if ((cadena.charAt(i) == 't' && cadena.charAt(i + 1) == 'a') && (cadena.charAt(i + 2) == 'n')) {
                i += 3;
                pila_operadores.add("tan");
            } else if ((cadena.charAt(i) == 'l' && cadena.charAt(i + 1) == 'o') && (cadena.charAt(i + 2) == 'g')) {
                i += 3;
                pila_operadores.add("log");
            }
            if (cadena.charAt(i) == 'p' && cadena.charAt(i + 1) == 'i') {
                pila_operadores.add("pi");
                i += 2;
            }
            pila_operadores.add(cadena.charAt(i));

        }

        System.out.println(pila_operadores);

        while (!pila_operadores.isEmpty()) {
            pila_reverso.add(pila_operadores.pop());
        }

        System.out.println(pila_reverso);

    }

    public void proceso_pilas() {
        //El puto procedimiento para acomodo de pilas automatron 2.0

        while (!pila_reverso.isEmpty()) {
            // En caso de ser operadores
            for (String fi[] : operadores) {
                for (String co : fi) {
                    if (co.equals(pila_reverso.lastElement().toString())) {
                        ipo(co);
                        System.out.println();
                    }
                }
            }

            //En caso de ser numerico
            if (isNumeric(pila_reverso.lastElement().toString())) {
                // System.out.println("numeros: " + pila_reverso.lastElement().toString());
            }

            //Si son otras variables
            for (String _var : variables) {
                if (_var.equals(pila_reverso.lastElement().toString())) {
                    // System.out.println("Variables: " + pila_reverso.lastElement().toString());
                }
            }

            pila_reverso.pop();
        }

    }

    Stack punk = new Stack();
    int dd = 0;

    //Orden de operadores 
    public void ipo(String elemento) {

        fila = 0;

        for (String fi[] : operadores) {
            fila += 1;
            for (String co : fi) {
                if (co.equals(elemento)) {

                    System.out.println("Caracter " + co + " corresponde a la fila " + fila);
                    System.out.println("Anterior: " + anterior + " Actual " + fila);
                    System.out.println("Cadena: " + cadena);
                    //Cuando es la priemra vez que se ingresa un operador
                    if (pila_temp.isEmpty()) {
                        pila_temp.add(co);
                        anterior = 0;
                    } else {
                        //Fila siempre debe ser mas grande que Anterior
                        if (fila > anterior) {
                            pila_temp.add(co);

                            //Si no lo es sacara el ultimo elemento e ingresara el nuevo mas grande
                        } else if (fila <= anterior) {

                            pila_temp.pop();

                            if (co.equals("+")) {
                                pila_temp.pop();
                            }

                            pila_temp.push(co);

                        }

                    }

                    anterior = fila;
                    punk.add(fila);

                    System.out.println(pila_temp);
                    System.out.println(punk);

//                    int res = 0;
//                    for (String b[] : operadores) {
//                        res += 1;
//                        for(String c : b){
//                            if(c.equals(co)){
//                                fila = res;
//                            }
//                        }
//                    }
//                    if (fila <= anterior) {
//                        
//                        pila_temp.pop();
//                        pila_temp.add(co);
//                        
//                        
//
//                    } else if (fila > anterior) {
//                        pila_temp.add(co);
//                    }
//                    
//                   
//
//                    System.out.println(pila_temp);
//                    System.out.println("fila: " + fila + " anterior: " + anterior);
//
//                    if (pila_temp.lastElement().equals(")")) {
//                        while (!pila_temp.lastElement().equals("(")) {
//
//                            System.out.println("Sacando: " + pila_temp.pop());
//
//                        }
//
//                        System.out.println("Sacando: " + pila_temp.pop()); //Ya no debe de aparecer al final
//                    }
//
//                    if (pila_temp.lastElement().equals("(")) {
//                        fila = 0;
//                       
//                    }
//
//                    
                }

            }

        }

    }

    //Determinar si es numerico
    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public void sistema() {
        this.conversor_caracteres();
        this.proceso_pilas();

    }

}
