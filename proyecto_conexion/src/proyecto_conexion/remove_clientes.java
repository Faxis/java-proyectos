package proyecto_conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class remove_clientes {
    //Datos Postgresql

    String Url = "jdbc:postgresql://localhost:5432/proyecto";
    String Driver = "org.postgresql.Driver";
    String Usuario = "postgres";
    String Clave = "bridama7175";

    public remove_clientes() {
        //Conexion a la DB_POSTGRESQL
        Connection con = null;

        try {

            Class.forName(Driver);

            try {

                //Segunda Consulta
                con = DriverManager.getConnection(Url, Usuario, Clave);

                String Solicitud = "TRUNCATE TABLE cliente CASCADE";

                PreparedStatement preparar = con.prepareStatement(Solicitud);

                ResultSet consulta2 = preparar.executeQuery();

                consulta2.close();
                con.close();

            } catch (SQLException e) {
                System.out.println(e.getMessage());
            } finally {

                JOptionPane.showMessageDialog(null, "Se han eliminado todos los clientes de la DB (Ten en cuenta que tambien elimina los registros de linea de detalle y compras)");
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}
