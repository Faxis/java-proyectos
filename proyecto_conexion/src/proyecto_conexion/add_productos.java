package proyecto_conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class add_productos {
    //Datos Postgresql

    String Url = "jdbc:postgresql://localhost:5432/proyecto";
    String Driver = "org.postgresql.Driver";
    String Usuario = "postgres";
    String Clave = "bridama7175";

    public add_productos(String produc[]) {
        //Conexion a la DB_POSTGRESQL
        Connection con = null;

        try {

            Class.forName(Driver);

            try {

                //Segunda Consulta
                con = DriverManager.getConnection(Url, Usuario, Clave);

                String Solicitud = "INSERT INTO producto VALUES (?,?,?,?,?,?)";

                PreparedStatement preparar = con.prepareStatement(Solicitud);

                preparar.setInt(1, Integer.parseInt(produc[0])); //Codigo Barras
                preparar.setString(2, produc[2]); //Categoria
                preparar.setInt(3, Integer.parseInt(produc[5])); //Productos en Existencia
                preparar.setInt(4, Integer.parseInt(produc[4])); //Precio Neto
                preparar.setString(5, produc[3]); //Descripcion
                preparar.setString(6, produc[1]); //Nombre

                ResultSet consulta2 = preparar.executeQuery();

                consulta2.close();
                con.close();

            } catch (SQLException e) {
                System.out.println(e.getMessage());
            } finally {

                JOptionPane.showMessageDialog(null, "EL registro se a dado con Exito en la DB");
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}
