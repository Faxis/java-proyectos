package proyecto_conexion;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class so_usuarios extends JFrame implements ActionListener {

    //Datos Postgresql
    String Url = "jdbc:postgresql://localhost:5432/proyecto";
    String Driver = "org.postgresql.Driver";
    String Usuario = "postgres";
    String Clave = "bridama7175";

    //Iconos
    ImageIcon x = new ImageIcon("add.png");
    ImageIcon y = new ImageIcon("return.png");

    //Botones
    JButton botonAdd = new JButton(x);
    JButton botonRe = new JButton(y);
    JButton botonRemoveAll = new JButton("Borrar Usuarios");

    //Todo para tabla
    JTable tbl = new JTable();
    JScrollPane panel = new JScrollPane(tbl);
    String culumna[] = {"Id Usuario", "Nombre", "Contraseña"};
    String fila[][];
    DefaultTableModel modelo = new DefaultTableModel(fila, culumna);

    //EL JMenu acerca del Sistema de Us
    JMenuBar barra = new JMenuBar();
    JMenuItem s1 = new JMenuItem("About us");

    int idContador;

    public so_usuarios() {

//Conexion a la DB_POSTGRESQL
        Connection con = null;
        try {

            Class.forName(Driver);

            try {
                //Primer Consulta @Go motherfucker JUMP
                con = DriverManager.getConnection(Url, Usuario, Clave);

                String Solicitud = "SELECT * FROM usuarios";

                PreparedStatement preparar = con.prepareStatement(Solicitud);
                ResultSet consulta = preparar.executeQuery();

                //Se declara el modelo de la tabla
                tbl.setModel(modelo);
                while (consulta.next()) {
                    String filas[] = {consulta.getString("id_usuario"), consulta.getString("usuario"), consulta.getString("clave")};

                    modelo.addRow(filas);

                    idContador = consulta.getInt(1);
                }

                consulta.close();

            } catch (SQLException e) {
                System.out.println(e.getMessage());
            } finally {

                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }

        // Fucking Interfaz we! @@By Arfaxad Zadot 
        //Declaracion del JPanel como interfaz
        JPanel Cristal = new JPanel();

        super.setVisible(true);
        super.setTitle("Administrar Usuarios");
        super.setSize(850, 580);
        super.setDefaultCloseOperation(EXIT_ON_CLOSE);
        super.setResizable(false);

        //Posiciones de Graficos
        botonAdd.setBounds(60, 450, 130, 40);
        botonRe.setBounds(200, 450, 130, 40);
        botonRemoveAll.setBounds(340, 450, 130, 40);

        barra.setBounds(0, 0, 850, 25);
        barra.add(s1);

        //SetTolTips
        botonAdd.setToolTipText("Agregar Nuevo Registro");
        botonRe.setToolTipText("Regresar al sistema de Citas");

        /* Agrego la fila al vector que contiene todas las filas */
 /* Creo una instancia de JScrollPane y le paso como parametro la tabla */
        panel.setBounds(40, 40, 750, 230);
        /* Por ultimo agrego ese objeto de JScrollPane al contenedor de la ventana */
        this.getContentPane().add(panel);
        tbl.setEnabled(false);

        //Agregados al Cristal
        Cristal.add(botonAdd);
        Cristal.add(botonRe);
        Cristal.add(botonRemoveAll);
        Cristal.add(barra);

        super.add(Cristal);

        Cristal.setLayout(null);

        //Escuchas
        botonRemoveAll.addActionListener(this);
        botonAdd.addActionListener(this);
        botonRe.addActionListener(this);
        s1.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        idContador++;
        int idContadorFinal = idContador;

        if (e.getSource() == botonAdd) {

            String Us = JOptionPane.showInputDialog("Ingresa Nombre de nuevo Usuario");
            String Pa = JOptionPane.showInputDialog("Ingrese la contraseña");
            String Pas = JOptionPane.showInputDialog("Confirmar contraseña");

            if (Pa.equals(Pas)) {
                String filas[] = {Integer.toString(idContadorFinal), Us, Pa};

                modelo.addRow(filas);

                add_usuario go = new add_usuario(idContadorFinal, Us, Pa);
            } else {

                JOptionPane.showMessageDialog(null, "La confirmacion de la contraseña no coinciden");

            }

        } else if (e.getSource() == s1) {
            JOptionPane.showMessageDialog(null, "Sistema de Usuarios desarollado por Team Piedra v1");

        } else if (e.getSource() == botonRemoveAll) {
            remove_usuarios re = new remove_usuarios();
            int conta = modelo.getRowCount() - 1;

            while (conta >= 0) {
                modelo.removeRow(conta);

                conta--;
            }

        } else if (e.getSource() == botonRe) {
            super.setVisible(false);
            menu foo = new menu();
        }

    }
}
