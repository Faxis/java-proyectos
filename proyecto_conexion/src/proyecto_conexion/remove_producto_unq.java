package proyecto_conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class remove_producto_unq extends javax.swing.JFrame {

    //Datos Postgresql
    String Url = "jdbc:postgresql://localhost:5432/proyecto";
    String Driver = "org.postgresql.Driver";
    String Usuario = "postgres";
    String Clave = "bridama7175";

    boolean gh = false;

    public remove_producto_unq() {

        initComponents();
        super.setVisible(true);
        super.setResizable(false);
        super.setTitle("Eliminar Producto");

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Ingresa el Codigo de Barras del producto para ser eliminado");

        jButton1.setText("Eliminar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel2.setText("    ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(164, 164, 164)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(112, 112, 112)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField1)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE))))
                .addContainerGap(66, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addComponent(jLabel1)
                .addGap(41, 41, 41)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        //Conexion a la DB_POSTGRESQL
        Connection con = null;

        try {

            Class.forName(Driver);

            try {

                //Segunda Consulta
                con = DriverManager.getConnection(Url, Usuario, Clave);

                String Solicitud = "SELECT codigo_barras FROM producto";

                PreparedStatement preparar = con.prepareStatement(Solicitud);

                try (ResultSet consulta2 = preparar.executeQuery()) {
                    jLabel2.setText("");
                    
                    while (consulta2.next()) {
                        
                        if (consulta2.getInt("codigo_barras") == Integer.parseInt(jTextField1.getText())) {
                            //ESTO ES PARA ELIMINAR DATOS
                            Connection con2 = null;
                            
                            try {
                                
                                Class.forName(Driver);
                                
                                try {
                                    
                                    //Segunda Consulta
                                    con2 = DriverManager.getConnection(Url, Usuario, Clave);
                                    
                                    String Solicitud2 = "DELETE FROM producto WHERE codigo_barras = ?";
                                    
                                    PreparedStatement preparar2 = con2.prepareStatement(Solicitud2);
                                    
                                    preparar2.setInt(1, Integer.parseInt(jTextField1.getText()));
                                    
                                    ResultSet consulta22 = preparar2.executeQuery();
                                    
                                    consulta22.close();
                                    con2.close();
                                    
                                } catch (SQLException e) {
                                    System.out.println(e.getMessage());
                                } finally {
                                    
                                    JOptionPane.showMessageDialog(null, "Se ha eliminado con exito");
                                    if (con != null) {
                                        try {
                                            con.close();
                                        } catch (SQLException e) {
                                            System.out.println(e.getMessage());
                                        }
                                    }
                                }
                            } catch (ClassNotFoundException e) {
                                System.out.println(e.getMessage());
                            }
                            
                            gh = true;
                            super.setVisible(false);
                            so_productos ggg = new so_productos();
                            
                        }

                    }

                    //Aqui vamos a ingresar el texto
                    if (gh == false) {
                        jLabel2.setText("El codigo de barras " + jTextField1.getText() + " No se encontro en la DB");
                    }
                }
                con.close();

            } catch (SQLException e) {
                System.out.println(e.getMessage());
            } finally {

                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(remove_producto_unq.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(remove_producto_unq.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(remove_producto_unq.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(remove_producto_unq.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new remove_producto_unq().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
