package proyecto_conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class add_usuario {
    //Datos Postgresql

    String Url = "jdbc:postgresql://localhost:5432/proyecto";
    String Driver = "org.postgresql.Driver";
    String Usuario = "postgres";
    String Clave = "bridama7175";

    public add_usuario(int x, String y, String z) {
        //Conexion a la DB_POSTGRESQL
        Connection con = null;

        try {

            Class.forName(Driver);

            try {

                //Segunda Consulta
                con = DriverManager.getConnection(Url, Usuario, Clave);

                String Solicitud = "INSERT INTO usuarios VALUES (?,?,?)";

                PreparedStatement preparar = con.prepareStatement(Solicitud);

                preparar.setInt(1, x);
                preparar.setString(2, y);
                preparar.setString(3, z);

                ResultSet consulta2 = preparar.executeQuery();


                consulta2.close();
                con.close();

            } catch (SQLException e) {
                System.out.println(e.getMessage());
            } finally {

                JOptionPane.showMessageDialog(null, "EL registro se a dado con Exito en la DB");
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}
