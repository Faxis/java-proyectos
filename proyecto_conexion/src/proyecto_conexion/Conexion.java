package proyecto_conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Conexion {

    public Conexion() {

        String Url = "jdbc:postgresql://localhost:5432/proyecto";
        String Driver = "org.postgresql.Driver";
        String Usuario = "postgres";
        String Clave = "bridama7175";

        Connection con = null;
        Connection con2 = null;

        try {

            Class.forName(Driver);

            try {
                con = DriverManager.getConnection(Url, Usuario, Clave);

                String Solicitud = "SELECT * FROM cliente";

                PreparedStatement preparar = con.prepareStatement(Solicitud);
                ResultSet consulta = preparar.executeQuery();

                String Nombre[];
                String Pass[];

                int contador = 0;

                while (consulta.next()) {
                    contador++;
                    System.out.println(contador);
                }
                Nombre = new String[contador + 1];
                Pass = new String[contador + 1];
                
                consulta.close();

                con2 = DriverManager.getConnection(Url, Usuario, Clave);

                String Solicitud2 = "SELECT * FROM cliente";

                PreparedStatement preparar2 = con2.prepareStatement(Solicitud);
                ResultSet consulta2 = preparar2.executeQuery();
                while (consulta2.next()) {
                    System.out.println(consulta2.getInt(1) + consulta2.getString("nombre"));

                }

            } catch (SQLException e) {
                System.out.println(e.getMessage());
            } finally {

                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}
